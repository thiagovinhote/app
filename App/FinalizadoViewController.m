//
//  FinalizadoViewController.m
//  Pomodoro
//
//  Created by Thiago Vinhote on 08/04/16.
//  Copyright © 2016 Thiago Vinhote. All rights reserved.
//

#import "FinalizadoViewController.h"
#import "Pomodoro.h"
@interface FinalizadoViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *imagem;
@property (weak, nonatomic) IBOutlet UILabel *labeTexto;

@end

@implementation FinalizadoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    int t = [self contar:self.atividade.pomodoros];
    if (t == 0) {
        self.imagem.image = [UIImage imageNamed:@"tomate a"];
        self.labeTexto.text = @"Boa! Você terminou a missão sem distrações!";
    }else if(t <= [self.atividade.pomodoros count]*0.5){
        self.imagem.image = [UIImage imageNamed:@"tomate b"];
        self.labeTexto.text = @"Poucas distrações! Você tá quase lá!";
    }else{
        self.imagem.image = [UIImage imageNamed:@"podre gigante"];
        self.labeTexto.text = @"Você conseguiu! Mas se distraiu muito... Vamos melhorar isso!";
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(int) contar:(RLMArray *)pomodoros{
    int total = 0;
    for(Pomodoro *p in pomodoros){
        if (p.status == false) {
            total++;
        }
    }
    return total;
}

- (IBAction)tabBar:(id)sender {
    
}

@end
