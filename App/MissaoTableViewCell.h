//
//  MissaoTableViewCell.h
//  Pomodoro
//
//  Created by Thiago Vinhote on 08/04/16.
//  Copyright © 2016 Thiago Vinhote. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MissaoTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *labelText;
@property (weak, nonatomic) IBOutlet UIImageView *imagem;
@property (weak, nonatomic) IBOutlet UIView *mainView;

@end
