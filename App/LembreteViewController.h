//
//  LembreteViewController.h
//  App
//
//  Created by Thiago Vinhote on 06/04/16.
//  Copyright © 2016 Thiago Vinhote. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LembreteViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)buttonPlus:(id)sender;

@end
