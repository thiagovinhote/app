//
//  NovaAtividadeController.h
//  App
//
//  Created by Thiago Vinhote on 05/04/16.
//  Copyright © 2016 Thiago Vinhote. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NovaAtividadeController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *textNome;
@property (weak, nonatomic) IBOutlet UITextField *textIdade;

@end
