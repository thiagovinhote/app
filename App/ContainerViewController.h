//
//  ContainerViewController.h
//  Pomodoro
//
//  Created by Thiago Vinhote on 08/04/16.
//  Copyright © 2016 Thiago Vinhote. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Realm/Realm.h>

@interface ContainerViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>

@property(strong, nonatomic) RLMArray *pomodori;

@end
