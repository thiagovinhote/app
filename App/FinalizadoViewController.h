//
//  FinalizadoViewController.h
//  Pomodoro
//
//  Created by Thiago Vinhote on 08/04/16.
//  Copyright © 2016 Thiago Vinhote. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Atividade.h"
@interface FinalizadoViewController : UIViewController

@property(strong, nonatomic) Atividade *atividade;
@end
