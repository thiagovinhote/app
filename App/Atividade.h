//
//  Atividade.h
//  App
//
//  Created by Thiago Vinhote on 05/04/16.
//  Copyright © 2016 Thiago Vinhote. All rights reserved.
//

#import <Realm/Realm.h>
#import "Pomodoro.h"

@interface Atividade : RLMObject

@property NSString *uuid;
@property NSString *nome;
@property NSDate *dataCriacao;
@property NSDate *dataFim;
@property bool status;
@property RLMArray<Pomodoro *><Pomodoro> *pomodoros;

@end

RLM_ARRAY_TYPE(Atividade);