//
//  Lembrete.h
//  App
//
//  Created by Thiago Vinhote on 06/04/16.
//  Copyright © 2016 Thiago Vinhote. All rights reserved.
//

#import <Realm/Realm.h>

@interface Lembrete : RLMObject

@property NSString *uuid;
@property NSString *nome_tarefa;
@property NSDate *data;
@property int prioridade;

@end

RLM_ARRAY_TYPE(Lembrete);
