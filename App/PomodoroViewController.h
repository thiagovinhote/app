//
//  PomodoroViewController.h
//  App
//
//  Created by Thiago Vinhote on 06/04/16.
//  Copyright © 2016 Thiago Vinhote. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Atividade.h"
#import "MCPercentageDoughnutView.h"

@interface PomodoroViewController : UIViewController<UICollectionViewDelegate, UICollectionViewDataSource, UIAlertViewDelegate>


@property (weak, nonatomic) IBOutlet MCPercentageDoughnutView *percentage;

@property (strong, nonatomic) Atividade *atividade;
@property (weak, nonatomic) IBOutlet UILabel *labelDataCriacao;
@property (weak, nonatomic) IBOutlet UILabel *labelStatus;
@property (weak, nonatomic) IBOutlet UILabel *labelDataFim;
@property (weak, nonatomic) IBOutlet UILabel *labelAtividade;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UICollectionView *collection;

@end
