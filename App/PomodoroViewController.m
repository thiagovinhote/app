//
//  PomodoroViewController.m
//  App
//
//  Created by Thiago Vinhote on 06/04/16.
//  Copyright © 2016 Thiago Vinhote. All rights reserved.
//

#import "PomodoroViewController.h"
#import "Pomodoro.h"
#import "FinalizadoViewController.h"
#import "ContainerViewController.h"
#import "TomateCollectionViewCell.h"
#import "SoundManager.h"
#import "ViewController.h"
#define MINUTOS 0.2
#define SEGUNDOS 60
#define MUSICA "Alarm_Clock.mp3"

@interface PomodoroViewController ()
@property (weak, nonatomic) IBOutlet UILabel *label;
@property(strong, nonatomic) NSTimer *countDown;
@property(strong, nonatomic) NSTimer *tempoVibracao;
@property int tempoTotal;
@property int segundos;
@property (strong, nonatomic) Pomodoro *pomodoro;
@property (weak, nonatomic) IBOutlet UIButton *btnPlay;
@property (weak, nonatomic) IBOutlet UIButton *btnVacilei;
@property (weak, nonatomic) IBOutlet UIButton *btnConsegui;
@end

@implementation PomodoroViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.label.text = @"";
    
    self.labelAtividade.text = self.atividade.nome;
    
    self.percentage.textStyle = MCPercentageDoughnutViewTextStyleUserDefined;
    self.percentage.percentage = 1;
    self.percentage.linePercentage = 0.04;
    self.percentage.animationDuration = 1;
    self.percentage.showTextLabel = true;
//    self.percentage.animatesBegining = YES;
    self.percentage.tintColor = [UIColor whiteColor];
    self.percentage.unfillColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0];
//    self.percentage.textLabel.text = [NSString stringWithFormat:@"%dm %ds", minuto, segundo];
    self.percentage.textLabel.textColor = [UIColor whiteColor];
    [self.btnVacilei setHidden:true];
    [self.btnConsegui setHidden:true];
    
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (self.atividade != nil) {
        @try {
            RLMResults *r = [Atividade objectsWhere:[NSString stringWithFormat:@"uuid == '%@'", self.atividade.uuid]];
            if (r.count == 0) {
                self.atividade = nil;
                [self stop:nil];
            }
        } @catch(NSException *theException) {
            self.atividade = nil;
            NSLog(@"%@\n\n%@", theException.name, theException.reason);
            [self stop:nil];
            [self limpar:@"Missão cancelada!"];
            [self iniciar:nil];
            [self.btnVacilei setHidden:true];
            [self.btnConsegui setHidden:true];
        }
    }else{
        [self limpar:@""];
        [self.label setText:@"Deixa de moleza!"];
        [self.labelAtividade setText:@"Ninguém sobe o morro!"];
    }
}

- (IBAction)iniciar:(id)sender {
    if (self.atividade == nil) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ops" message:@"Primeiro vamos decidir qual a missão!" delegate:self cancelButtonTitle:@"Tá" otherButtonTitles:nil, nil];
        [alert setTag:80];
        [alert show];
    }else{
        [self setTimer:MINUTOS msg:@"Trabalhe! Vai!"];
        [self addPomodoro:nil];
        [self.btnPlay setHidden:true];
        [self.btnVacilei setHidden:false];
        [self.btnConsegui setHidden:false];
        [self.btnVacilei setEnabled:true];
    }
    
//    [MCUtil runOnAuxiliaryQueue:^{
//        while (timeInMiliseconds <= tempo)
//        {
//            [MCUtil runOnMainQueue:^{
//                [self countDown:&tempo];
//            }];
//            [NSThread sleepForTimeInterval:1];
//        }
//    }];

//    [self startTimer];
}



-(void) setTimer:(float)minutos msg:(NSString *)msg{
    self.label.text = msg;
    self.segundos = self.tempoTotal = minutos * SEGUNDOS;
    self.countDown = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timerRun) userInfo:nil repeats:YES];
}

-(void) setTimerIntervalo:(float)minutos msg:(NSString *)msg{
    self.label.text = msg;
    self.segundos = self.tempoTotal = minutos * SEGUNDOS;
    self.countDown = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timerRunIntervalo) userInfo:nil repeats:YES];
}

-(void) timerRun{
    self.segundos--;
    int minutos = self.segundos / SEGUNDOS;
    int segundos = self.segundos - (minutos * SEGUNDOS);
    
    NSString *string = [NSString stringWithFormat:@"%02dm %02ds", minutos, segundos];
    self.percentage.textLabel.text = string;
    self.percentage.percentage = (float)(1.0 - ((self.segundos * 100)/self.tempoTotal)/100.0);
    if (self.segundos == 0) {
        [self stop:true];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Show" message:@"Hora de tirar a bandoleira!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert setTag:100];
        [alert show];
        [self somAlarme];
        //[self vibrar];
    }
}

-(void) timerRunIntervalo{
    self.segundos--;
    int minutos = self.segundos / SEGUNDOS;
    int segundos = self.segundos - (minutos * SEGUNDOS);
    
    NSString *string = [NSString stringWithFormat:@"%02dm %02ds", minutos, segundos];
    self.percentage.textLabel.text = string;
    self.percentage.percentage = (float)(1.0 - ((self.segundos * 100)/self.tempoTotal)/100.0);
    if (self.segundos == 0) {
        [self limpar:@""];
        [self.countDown invalidate];
        self.countDown = nil;
        self.label.text = @"Vamos lá! Mais um pomodoro!";
        [self.btnPlay setHidden:false];
        [self.btnVacilei setHidden:true];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Olá" message:@"Já tá bom, acabou o descanso!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert setTag:90];
        [alert show];
        [self vibrar];
        [SoundManager sharedManager].allowsBackgroundMusic = YES;
        [[SoundManager sharedManager] prepareToPlay];
        [[SoundManager sharedManager] playMusic:@MUSICA];
    }
}

-(void)somAlarme{
    [SoundManager sharedManager].allowsBackgroundMusic = YES;
    [[SoundManager sharedManager] prepareToPlay];
    [[SoundManager sharedManager] playMusic:@MUSICA looping:YES];
    [self vibrar];
}

-(void)vibrar{
    self.tempoVibracao = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(runVibrar) userInfo:nil repeats:YES];
}

-(void)runVibrar{
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    [[SoundManager sharedManager]stopMusic];
    [self.tempoVibracao invalidate];
    self.tempoVibracao = nil;
    if (buttonIndex == 0) {
        if (alertView.tag == 100) {
            if (([self.atividade.pomodoros count] % 4) == 0) {
                [self setTimerIntervalo:0.1 msg:@"Já trabalhou o bastante!\nRelaxe um pouco"];
            }else{
                
                [self setTimerIntervalo:0.05 msg:@"Vamos dar uma pausa"];
            }
        }
        if (alertView.tag == 80) {
            UITabBarController *tabBar = self.tabBarController;
            ViewController *vc = [tabBar.viewControllers objectAtIndex:0];
            tabBar.selectedViewController = vc;
        }

    }
    
    
//    UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    TestViewController *obj1=[storyboard instantiateViewControllerWithIdentifier:@"View Controller"];
//    [obj1 handleNotificationMessage: NSSMessage];
}


-(void)stop:(bool) status{
    [self.countDown invalidate];
    self.countDown = nil;
    if (self.atividade != nil) {
        RLMRealm *realm = [RLMRealm defaultRealm];
        [realm beginWriteTransaction];
        self.pomodoro.final = [[NSDate date] timeIntervalSinceReferenceDate];
        self.pomodoro.status = status;
        [realm commitWriteTransaction];
        NSLog(@"%@", self.atividade);
        [self.collection reloadData];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)limpar:(NSString *)msg{
    self.percentage.textLabel.text = msg;
    self.percentage.percentage = 1;
}

- (IBAction)Vacilei:(id)sender {
    [self stop:false];
    [self limpar:@""];
    [self.countDown invalidate];
    self.countDown = nil;
    self.label.text = @"Não acredito vey! \nVamos lá!!";
    [self.btnPlay setHidden:false];
    [self.btnVacilei setEnabled:false];
}

- (IBAction)Terminel:(id)sender {
    RLMRealm *realm = [RLMRealm defaultRealm];
    [self.countDown invalidate];
    self.countDown = nil;
    [self.tempoVibracao invalidate];
    self.tempoVibracao = nil;
    if (self.pomodoro.final == 0) {
        [self stop:true];
    }
    [realm beginWriteTransaction];
    self.atividade.status = true;
    [realm commitWriteTransaction];
}

- (IBAction)addPomodoro:(id)sender {
    NSLog(@"%@", self.atividade);
    RLMRealm *defaultRealm = [RLMRealm defaultRealm];
    
    Pomodoro *pomodoro = [[Pomodoro alloc] init];
    
    pomodoro.status = true;
    pomodoro.inicial = [[NSDate date] timeIntervalSinceReferenceDate];
    pomodoro.final = 0;
    
    [defaultRealm beginWriteTransaction];
    [self.atividade.pomodoros addObject:pomodoro];
    [defaultRealm addObject:self.atividade];
    [defaultRealm commitWriteTransaction];
    
    self.pomodoro = pomodoro;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"finalizado"]) {
        FinalizadoViewController *vc = [segue destinationViewController];
        [vc setAtividade:self.atividade];
    }
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [self.atividade.pomodoros count];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    Pomodoro *p = self.atividade.pomodoros[indexPath.row];
    
    TomateCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];

    UIImage *image;
    if (p.status == true) {
        image = [UIImage imageNamed:@"tomate-crono"];
    }else{
        image = [UIImage imageNamed:@"podre"];
    }
    cell.imagem.image = image;
    
    return cell;
}


@end
