//
//  TomateCollectionViewCell.h
//  Pomodoro
//
//  Created by Thiago Vinhote on 09/04/16.
//  Copyright © 2016 Thiago Vinhote. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TomateCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imagem;

@end
