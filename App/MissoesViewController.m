//
//  MissoesViewController.m
//  Pomodoro
//
//  Created by Thiago Vinhote on 07/04/16.
//  Copyright © 2016 Thiago Vinhote. All rights reserved.
//

#import "MissoesViewController.h"
#import <Realm/Realm.h>
#import "Atividade.h"
#import "Pomodoro.h"
#import "MissaoConcluidaTableViewCell.h"
#import "TomateCollectionViewCell.h"

@interface MissoesViewController ()

@property(strong, nonatomic) RLMResults *atividades;
@end

@implementation MissoesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.atividades = [[Atividade allObjects] objectsWhere:@"status == true"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    NSInteger numOfSections = 0;
    if ([self.atividades count] > 0)
    {
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        numOfSections = 1;
        tableView.backgroundView   = nil;
    }
    else
    {
        UILabel *noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, tableView.bounds.size.height+30)];
        noDataLabel.alpha = 0;
        [noDataLabel setNumberOfLines:2];
        noDataLabel.text             = @"Sem missões concluídas!! \nMude isso!";
        noDataLabel.textColor        = [UIColor whiteColor];
        noDataLabel.textAlignment    = NSTextAlignmentCenter;
        [UIView animateWithDuration:1 animations:^{
            tableView.backgroundView = noDataLabel;
            noDataLabel.alpha = 1;
        }];
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    
    return numOfSections;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.atividades count];
}

-(long)totalTempo:(RLMArray *)pomodoros{
    
    long int total = 0;
    
    for(Pomodoro *p in pomodoros){
        long valor = p.final - p.inicial;
        total += valor;
    }
    
    return total;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    MissaoConcluidaTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"missaoConcluida"];
    
    Atividade *a = [self.atividades objectAtIndex:indexPath.row];
    
    //Pomodoro *p = a.pomodoros[0];
    
//    NSDate *data = [NSDate dateWithTimeIntervalSinceNow:p.inicial];
//    NSDate *fim = [data dateByAddingTimeInterval:[self totalTempo:a.pomodoros]];
    
//    NSTimeInterval tInicial = [[NSDate date] timeIntervalSinceDate:data];
//    NSTimeInterval tFim = [[NSDate date] timeIntervalSinceDate:fim];
    
//    NSTimeInterval d = tInicial - tFim;
    
    //NSTimeInterval intervalo = [fim timeIntervalSinceDate:data];
    
    //cell.textLabel.text = a.nome;
    
    NSTimeInterval intervalo = [self totalTempo:a.pomodoros];
    
    NSInteger ti = (NSInteger)intervalo;
    NSInteger seconds = ti % 60;
    NSInteger minutes = (ti / 60) % 60;
    NSInteger hours = (ti / 3600);
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0.0]];
    
    
    // Format the elapsed time and set it to the label
    NSString *timeString = [dateFormatter stringFromDate:a.dataCriacao];

    
    cell.labelNome.text = a.nome;
    cell.labelData.text = timeString;
    cell.labelTotalHoras.text = [NSString stringWithFormat:@"%02dh %02dm %02ds", hours, minutes, seconds];
    
    NSInteger podres = [[a.pomodoros objectsWhere:@"status == false"] count];
    NSInteger bom = [[a.pomodoros objectsWhere:@"status == true"] count];
    
    if (bom > 0) {
        cell.labelTomatesBom.text = [NSString stringWithFormat:@"%02ld", (long)bom];
    }else{
        [cell.labelTomatesBom setHidden:true];
        [cell.imgBom setHidden:true];
    }
    
    if (podres > 0) {
        cell.labelTomatesPodre.text = [NSString stringWithFormat:@"%02ld", (long)podres];
    }else{
        [cell.labelTomatesPodre setHidden:true];
        [cell.imgPodre setHidden:true];
    }
    
//    [cell.collection setDelegate:self];
//    [cell.collection setDataSource:self];
//    cell.collection.tag = indexPath.row;
    //cell.detailTextLabel.text = [NSString stringWithFormat:@"%ld", [self totalTempo:a.pomodoros]];
    //cell.detailTextLabel.text = [NSString stringWithFormat:@"%02d : %02d : %02d", hours, minutes, seconds];
    
    return cell;
}

//-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
//    return 1;
//}
//
//-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
//    Atividade *a = self.atividades[collectionView.tag];
//    return a.pomodoros.count;
//}
//
//-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
//    
//    TomateCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
//    
//    Atividade *a = self.atividades[collectionView.tag];
//    
//    Pomodoro *p = a.pomodoros[indexPath.row];
//    
//    UIImage *image;
//    if (p.status == true) {
//        image = [UIImage imageNamed:@"tomate-crono"];
//    }else{
//        image = [UIImage imageNamed:@"podre"];
//    }
//    cell.imagem.image = image;
//    
//    return cell;
//}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        RLMRealm *realm = [RLMRealm defaultRealm];
        [realm beginWriteTransaction];
        [realm deleteObject:[self.atividades objectAtIndex:indexPath.row]];
        [realm commitWriteTransaction];
        self.atividades = [[Atividade allObjects] objectsWhere:@"status == true"];
        [self.tableView reloadData];
//        [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row % 2 == 0) {
        UIView *cellContentView  = [cell contentView];
        CGFloat rotationAngleDegrees = 30;
        CGFloat rotationAngleRadians = rotationAngleDegrees * (M_PI/180);
        CGPoint offsetPositioning = CGPointMake(500, 20.0);
        CATransform3D transform = CATransform3DIdentity;
        transform = CATransform3DRotate(transform, rotationAngleRadians, 50.0, 0.0, 1.0);
        transform = CATransform3DTranslate(transform, offsetPositioning.x, offsetPositioning.y, 50.0);
        cellContentView.layer.transform = transform;
        cellContentView.layer.opacity = 0.8;
        
        [UIView animateWithDuration:.65 delay:0.0 usingSpringWithDamping:0.85 initialSpringVelocity:.8 options:0 animations:^{
            cellContentView.layer.transform = CATransform3DIdentity;
            cellContentView.layer.opacity = 1;
        } completion:^(BOOL finished) {}];
    }else{
        UIView *cellContentView = [cell contentView];
        CGFloat rotationAngleDegrees = 30;
        CGFloat rotationAngleRadians = rotationAngleDegrees * (M_PI/180);
        CGPoint offsetPositioning = CGPointMake(0, cell.contentView.frame.size.height*4);
        CATransform3D transform = CATransform3DIdentity;
        transform = CATransform3DRotate(transform, rotationAngleRadians, 50.0, 0.0, 1.0);
        transform = CATransform3DTranslate(transform, offsetPositioning.x, offsetPositioning.y, 50.0);
        cellContentView.layer.transform = transform;
        cellContentView.layer.opacity = 0.8;
        
        [UIView animateWithDuration:0.65 delay:00 usingSpringWithDamping:0.85 initialSpringVelocity:0.8 options:0 animations:^{
            cellContentView.layer.transform = CATransform3DIdentity;
            cellContentView.layer.opacity = 1;
        } completion:^(BOOL finished) {}];
    }
}


@end
