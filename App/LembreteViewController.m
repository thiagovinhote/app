//
//  LembreteViewController.m
//  App
//
//  Created by Thiago Vinhote on 06/04/16.
//  Copyright © 2016 Thiago Vinhote. All rights reserved.
//

#import "LembreteViewController.h"
#import "Lembrete.h"
#import "MissaoNPTableViewCell.h"

@interface LembreteViewController ()

@property(strong, nonatomic) RLMResults *lembretes;
@property(strong, nonatomic)Lembrete *lembrete;
@end

@implementation LembreteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.lembretes = [Lembrete allObjects];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}


- (IBAction)buttonPlus:(id)sender {
    
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Missão não planejada" message:@"Preencha como o melhor nome que puder!" delegate:self cancelButtonTitle:@"Cancelar" otherButtonTitles:@"Adicionar", nil];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alert textFieldAtIndex:0].placeholder = @"Ex: não invadir o morro!";
    alert.inputView.frame = CGRectMake(12.0, 45.0, 260, 25.0);
    [alert setTag:1];
    [alert show];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex == 1 && alertView.tag == 1) {
        self.lembrete = [Lembrete new];
        self.lembrete.nome_tarefa = [[alertView textFieldAtIndex:0] text];
        UIAlertView *alerta = [[UIAlertView alloc] initWithTitle:@"Prioridade!" message:nil delegate:self cancelButtonTitle:@"Cancelar" otherButtonTitles:@"Alta", @"Mais ou menos", @"Tanto faz", nil];
        [alerta setTag:2];
        [alerta show];
    }else if(buttonIndex != 0 && alertView.tag == 2){
        RLMRealm *save = [RLMRealm defaultRealm];
        self.lembrete.data = [NSDate date];
        self.lembrete.prioridade = (int)buttonIndex;
        
        [save transactionWithBlock:^{
            [save addObject:self.lembrete];
        }];
        
        self.lembretes = [Lembrete allObjects];
        [self.tableView reloadData];
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    NSInteger numOfSections = 0;
    if ([self.lembretes count] > 0)
    {
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        numOfSections = 1;
        tableView.backgroundView   = nil;
    }
    else
    {
        UILabel *noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, tableView.bounds.size.height+30)];
        noDataLabel.alpha = 0;
        [noDataLabel setNumberOfLines:2];
        noDataLabel.text             = @"Com nada não planejada!! \nVeremos!";
        noDataLabel.textColor        = [UIColor whiteColor];
        noDataLabel.textAlignment    = NSTextAlignmentCenter;
        [UIView animateWithDuration:1 animations:^{
            tableView.backgroundView = noDataLabel;
            noDataLabel.alpha = 1;
        }];
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    
    return numOfSections;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.lembretes.count;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    //UITableViewCell *celula = [tableView dequeueReusableCellWithIdentifier:@"missao"];
    MissaoNPTableViewCell *celula = [tableView dequeueReusableCellWithIdentifier:@"missaoNP"];
    Lembrete *lembrete = self.lembretes[indexPath.row];
    celula.labelNome.text = lembrete.nome_tarefa;
//    celula.detailTextLabel.text =  [NSString stringWithFormat:@"%@", lembrete.data];
    return celula;
    
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        RLMRealm *realm = [RLMRealm defaultRealm];
        [realm beginWriteTransaction];
        [realm deleteObject:[self.lembretes objectAtIndex:indexPath.row]];
        [realm commitWriteTransaction];
        [tableView reloadData];
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row % 2 == 0) {
        UIView *cellContentView = [cell contentView];
        CGFloat rotationAngleDegrees = 30;
        CGFloat rotationAngleRadians = rotationAngleDegrees * (M_PI/180);
        CGPoint offsetPositioning = CGPointMake(0, cell.contentView.frame.size.height*4);
        CATransform3D transform = CATransform3DIdentity;
        transform = CATransform3DRotate(transform, rotationAngleRadians, 50.0, 0.0, 1.0);
        transform = CATransform3DTranslate(transform, offsetPositioning.x, offsetPositioning.y, 50.0);
        cellContentView.layer.transform = transform;
        cellContentView.layer.opacity = 0.8;
        
        [UIView animateWithDuration:0.65 delay:00 usingSpringWithDamping:0.85 initialSpringVelocity:0.8 options:0 animations:^{
            cellContentView.layer.transform = CATransform3DIdentity;
            cellContentView.layer.opacity = 1;
        } completion:^(BOOL finished) {}];
    }else{
        UIView *cellContentView  = [cell contentView];
        CGFloat rotationAngleDegrees = 30;
        CGFloat rotationAngleRadians = rotationAngleDegrees * (M_PI/180);
        CGPoint offsetPositioning = CGPointMake(500, 20.0);
        CATransform3D transform = CATransform3DIdentity;
        transform = CATransform3DRotate(transform, rotationAngleRadians, 50.0, 0.0, 1.0);
        transform = CATransform3DTranslate(transform, offsetPositioning.x, offsetPositioning.y, 50.0);
        cellContentView.layer.transform = transform;
        cellContentView.layer.opacity = 0.8;
        
        [UIView animateWithDuration:.65 delay:0.0 usingSpringWithDamping:0.85 initialSpringVelocity:.8 options:0 animations:^{
            cellContentView.layer.transform = CATransform3DIdentity;
            cellContentView.layer.opacity = 1;
        } completion:^(BOOL finished) {}];
    }
}

@end
