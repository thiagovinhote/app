//
//  MissaoConcluidaTableViewCell.h
//  Pomodoro
//
//  Created by Thiago Vinhote on 08/04/16.
//  Copyright © 2016 Thiago Vinhote. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Realm/Realm.h>

@interface MissaoConcluidaTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *labelNome;
@property (weak, nonatomic) IBOutlet UILabel *labelData;
@property (weak, nonatomic) IBOutlet UILabel *labelTotalHoras;
//@property (weak, nonatomic) IBOutlet UICollectionView *collection;
@property (weak, nonatomic) IBOutlet UILabel *labelTomatesPodre;
@property (weak, nonatomic) IBOutlet UILabel *labelTomatesBom;
@property (weak, nonatomic) IBOutlet UIImageView *imgPodre;
@property (weak, nonatomic) IBOutlet UIImageView *imgBom;

@end
