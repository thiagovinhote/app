//
//  Lembrete.m
//  App
//
//  Created by Thiago Vinhote on 06/04/16.
//  Copyright © 2016 Thiago Vinhote. All rights reserved.
//

#import "Lembrete.h"

@implementation Lembrete


+ (NSString *)primaryKey{
    return @"uuid";
}

+(NSDictionary *) defaultPropertyValues{
    return @{@"uuid": [[NSUUID UUID] UUIDString], @"nome_tarefa": @""};
}

@end
