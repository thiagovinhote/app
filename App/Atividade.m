//
//  Atividade.m
//  App
//
//  Created by Thiago Vinhote on 05/04/16.
//  Copyright © 2016 Thiago Vinhote. All rights reserved.
//

#import "Atividade.h"

@implementation Atividade

+ (NSString *)primaryKey{
    return @"uuid";
}

+(NSDictionary *) defaultPropertyValues{
    return @{@"uuid": [[NSUUID UUID] UUIDString], @"nome": @""};
}
@end
