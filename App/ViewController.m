//
//  ViewController.m
//  App
//
//  Created by Thiago Vinhote on 05/04/16.
//  Copyright © 2016 Thiago Vinhote. All rights reserved.
//

#import "ViewController.h"
#import <Realm/Realm.h>
#import "Atividade.h"
#import "PomodoroViewController.h"
#import "MissaoTableViewCell.h"
#import <QuartzCore/QuartzCore.h>

@interface ViewController ()

@property RLMResults *atividades;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.tableView.rowHeight = UITableViewAutomaticDimension;
//    self.tableView.estimatedRowHeight = 100;
    self.atividades = [[Atividade allObjects] objectsWhere:@"status == false"];

}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.tableView reloadData];
    NSLog(@"%@", self.atividades);

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    NSInteger numOfSections = 0;
    if ([self.atividades count] > 0)
    {
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        numOfSections = 1;
        tableView.backgroundView   = nil;
    }
    else
    {
        UILabel *noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, tableView.bounds.size.height+30)];
        [noDataLabel setNumberOfLines:2];
        noDataLabel.alpha = 0;
        noDataLabel.text             = @"Com nenhuma missão planejada!!\nAinda!";
        noDataLabel.textColor        = [UIColor whiteColor];
        noDataLabel.textAlignment    = NSTextAlignmentCenter;
        [UIView animateWithDuration:1 animations:^{
            tableView.backgroundView = noDataLabel;
            noDataLabel.alpha = 1;
        }];
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    
    return numOfSections;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.atividades count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    MissaoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"missao"];
    Atividade *atividade = self.atividades[indexPath.row];
    cell.labelText.text = atividade.nome;
    return cell;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([[segue identifier] isEqualToString:@"pomodoro"]) {
        
        PomodoroViewController *vc = [segue destinationViewController];
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        
        [vc setAtividade:self.atividades[indexPath.row]];
    }
    
    if ([[segue identifier] isEqualToString:@"tab"]) {
        
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];

        UITabBarController *tabar = segue.destinationViewController;
        PomodoroViewController *vc =[tabar.viewControllers objectAtIndex:1];
        vc.atividade = self.atividades[indexPath.row];
        tabar.selectedViewController = vc;
    }
    
}

- (IBAction)addTarefa:(id)sender {
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Nova missão" message:@"Preencha como o melhor nome que puder!" delegate:self cancelButtonTitle:@"Cancelar" otherButtonTitles:@"Adicionar", nil];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alert textFieldAtIndex:0].placeholder = @"Ex: invadir o morro!";
    alert.inputView.frame = CGRectMake(12.0, 45.0, 260, 25.0);
    [alert show];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex == 1) {
        NSString *texto = [[alertView textFieldAtIndex:0] text];
        NSString *trim = [texto stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        if (![trim isEqualToString:@""]) {
            RLMRealm *defaultRealm = [RLMRealm defaultRealm];
            Atividade *atividade = [[Atividade alloc] init];
            
            atividade.nome = trim;
            atividade.dataCriacao = [NSDate date];
            atividade.status = false;
            
            [defaultRealm transactionWithBlock:^{
                [defaultRealm addObject:atividade];
            }];
            
            self.atividades = [[Atividade allObjects] objectsWhere:@"status == false"];
            
            //        UIAlertView *alerta = [[UIAlertView alloc] initWithTitle:@"Sucesso" message:@"Você acabou de adicionar uma nova atividade!" delegate:self cancelButtonTitle:@"Fechar" otherButtonTitles:nil, nil];
            //        [alerta show];
            
            [self.tableView reloadData];
        }
    }
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        RLMRealm *realm = [RLMRealm defaultRealm];
        [realm beginWriteTransaction];
        [realm deleteObject:[self.atividades objectAtIndex:indexPath.row]];
        [realm commitWriteTransaction];
        self.atividades = [[Atividade allObjects] objectsWhere:@"status == false"];
        [self.tableView reloadData];
        }
    
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row % 2 == 0) {
        UIView *cellContentView  = [cell contentView];
        CGFloat rotationAngleDegrees = -30;
        CGFloat rotationAngleRadians = rotationAngleDegrees * (M_PI/180);
        CGPoint offsetPositioning = CGPointMake(500, -20.0);
        CATransform3D transform = CATransform3DIdentity;
        transform = CATransform3DRotate(transform, rotationAngleRadians, -50.0, 0.0, 1.0);
        transform = CATransform3DTranslate(transform, offsetPositioning.x, offsetPositioning.y, -50.0);
        cellContentView.layer.transform = transform;
        cellContentView.layer.opacity = 0.8;
        
        [UIView animateWithDuration:.65 delay:0.0 usingSpringWithDamping:0.85 initialSpringVelocity:.8 options:0 animations:^{
            cellContentView.layer.transform = CATransform3DIdentity;
            cellContentView.layer.opacity = 1;
        } completion:^(BOOL finished) {}];
    }else{
        UIView *cellContentView = [cell contentView];
        CGFloat rotationAngleDegrees = -30;
        CGFloat rotationAngleRadians = rotationAngleDegrees * (M_PI/180);
        CGPoint offsetPositioning = CGPointMake(0, cell.contentView.frame.size.height*4);
        CATransform3D transform = CATransform3DIdentity;
        transform = CATransform3DRotate(transform, rotationAngleRadians, -50.0, 0.0, 1.0);
        transform = CATransform3DTranslate(transform, offsetPositioning.x, offsetPositioning.y, -50.0);
        cellContentView.layer.transform = transform;
        cellContentView.layer.opacity = 0.8;
    
        [UIView animateWithDuration:0.65 delay:00 usingSpringWithDamping:0.85 initialSpringVelocity:0.8 options:0 animations:^{
            cellContentView.layer.transform = CATransform3DIdentity;
            cellContentView.layer.opacity = 1;
        } completion:^(BOOL finished) {}];
    }
}
@end
