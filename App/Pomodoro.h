//
//  Pomodoro.h
//  App
//
//  Created by Thiago Vinhote on 06/04/16.
//  Copyright © 2016 Thiago Vinhote. All rights reserved.
//

#import <Realm/Realm.h>

@interface Pomodoro : RLMObject

//@property NSString *uuid;
@property long int inicial;
@property long int final;
@property bool status;

@end

RLM_ARRAY_TYPE(Pomodoro);
