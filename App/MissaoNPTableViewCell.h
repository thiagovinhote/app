//
//  MissaoNPTableViewCell.h
//  Pomodoro
//
//  Created by Thiago Vinhote on 11/04/16.
//  Copyright © 2016 Thiago Vinhote. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MissaoNPTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *labelNome;

@end
