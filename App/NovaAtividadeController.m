//
//  NovaAtividadeController.m
//  App
//
//  Created by Thiago Vinhote on 05/04/16.
//  Copyright © 2016 Thiago Vinhote. All rights reserved.
//

#import "NovaAtividadeController.h"
#import <Realm/Realm.h>
#import "Atividade.h"

@interface NovaAtividadeController ()

@end

@implementation NovaAtividadeController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)AddAtividade:(id)sender {
    RLMRealm *defaultRealm = [RLMRealm defaultRealm];
    Atividade *atividade = [[Atividade alloc] init];
    
    atividade.nome = self.textNome.text;
    atividade.dataCriacao = [NSDate date];
    atividade.status = false;
    
    [defaultRealm transactionWithBlock:^{
        [defaultRealm addObject:atividade];
    }];
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Realm Tutorial" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"Cancel",@"Save", nil];
    [alert show];
}


@end
